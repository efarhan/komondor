'''
Created on 11 dec. 2013

@author: efarhan
'''
import os
from os import listdir
from os.path import isfile, join
from engine.image_manager import load_image, load_image_with_size,\
    get_image_size
from engine.const import animation_step,path_prefix, log

class Animation():
    def __init__(self):
        self.img = 0
        self.path = ""
        self.state_range = {}
        self.path_list = []
        self.state = ""
        self.anim_counter = 0
        self.anim_speed = animation_step
        self.last_index = 0
        self.loop = True
    def load_images(self,size=None,permanent=False):
        self.img_indexes = []
        
        for p in self.path_list:
            path = path_prefix+self.path+p
            files = []
            if ".png" in path:
                files = [path]
            else:
                try:
                    files = [ os.path.join(path, f) for f in listdir(path) if (isfile(join(path, f)) and f.find(".png") != -1) ]
                except IOError:
                    return
            files.sort()
            for f in files:
                if size == None:
                    self.img_indexes.append(load_image(f,permanent))
                else:
                    self.img_indexes.append(load_image_with_size(f, size, permanent))
            self.img = self.img_indexes[0]
        self.size = get_image_size(self.img)
    def update_animation(self,state="",invert=False):
        if state != "":
            self.state = state
        if(self.anim_counter == self.anim_speed):
            anim_index = []
            if self.state_range == {}:
                anim_index = self.img_indexes
            else:
                try:
                    anim_index = self.img_indexes[self.state_range[self.state][0]:self.state_range[self.state][1]]
                    
                except KeyError:
                    log('KEYERROR ON STATERANGE')
                    return
            try:
                
                self.last_index = anim_index.index(self.img)
                
                if not invert:
                    self.img = anim_index[(self.last_index+1)%len(anim_index)]
                    self.last_index = (self.last_index+1)%len(anim_index)
                    
                else:
                    self.img = anim_index[(self.last_index-1)%len(anim_index)]
                    self.last_index = (self.last_index-1)%len(anim_index)
            except ValueError:
                try:
                    if not invert:
                        self.img = anim_index[(self.last_index+1)%len(anim_index)]
                        self.last_index = (self.last_index+1)%len(anim_index)
                    
                    else:
                        self.img = anim_index[(self.last_index-1)%len(anim_index)]
                        self.last_index = (self.last_index-1)%len(anim_index)
                except IndexError:
                    self.img = anim_index[0]
                    self.last_index = 0
                except ZeroDivisionError:
                    log("No images",1)
                    return
            except KeyError:
                try:
                    if not invert:
                        self.img = anim_index[(self.last_index+1)%len(anim_index)]
                        self.last_index = (self.last_index+1)%len(anim_index)
                    
                    else:
                        self.img = anim_index[(self.last_index-1)%len(anim_index)]
                        self.last_index = (self.last_index-1)%len(anim_index)
                except IndexError:
                    self.img = anim_index[0]
                    self.last_index = 0
            self.anim_counter = 0
        else:
            self.anim_counter += 1
