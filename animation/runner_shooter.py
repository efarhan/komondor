'''
Created on 1 mars 2014

@author: efarhan
'''
import math
from animation.player_animation import PlayerAnimation
from event.event_util import get_button
from event.physics_event import get_physics_event
from engine.physics import move, meter2pixel
from engine.const import pookoo, jump_step, jump_velocity, log
from engine.init import get_screen_size
from engine.stat import set_value, get_value
from engine.sound_manager import switch_music, set_music_index,\
    get_current_music, get_music_pos, load_sound, play_sound,\
    get_playlist_length
from engine.level_manager import get_level


jetpack_cool_const = 45

class RunShootAnimation(PlayerAnimation):
    def __init__(self, player):
        PlayerAnimation.__init__(self, player)
        self.jump_step = 0
        
        self.jump_velocity = jump_velocity
        self.touch_wall = False
        self.jump_click = False
        self.jump_clicked = False
        self.jetpack_click = False
        self.jetpack = False
        self.jetpack_once = False
        self.jetpackcool = jetpack_cool_const
        self.jetpack_sound = load_sound('data/sound/jetpack-mast.ogg')
        set_value("jet",self.jetpackcool)
        self.shoot_click = 0
        self.death = False
        self.switch_sound = load_sound('data/sound/switch.ogg')
        self.laser_sound = load_sound('data/sound/laser1-new.ogg')
        self.step_sounds = map((lambda i: load_sound('data/sound/pas%i.ogg'%(i))),range(6))
        self.step_index = 0
        self.metatouch = False
        self.touch_up = False
        self.in_air = False
    def death_handle(self):
        '''Show death text'''
        self.death = True
        self.player.show = False
        self.player.execute_event('on_death')
        self.jetpack_sound.stop()
        log('death')
    def move_horizontal(self,horizontal):
        if horizontal != 0:
            speed = 0.003
            new_x_pos = (self.player.screen_relative_pos[0]+horizontal*speed)
            if not(0.0 < new_x_pos < 0.9):
                return
            self.player.set_relative_pos(
                (new_x_pos,self.player.screen_relative_pos[1]))
    def update_state(self):
        if self.death:
            move(self.player.body,0,0)
            self.update_pos()
            return
        self.loop = True
        move(self.player.body, self.player.speed)    
        
        LEFT = get_button('player_left') or \
            get_button('player_left2')
        RIGHT = get_button('player_right') or \
            get_button('player_right2')
        horizontal = RIGHT-LEFT
        UP = get_button('player_up') or \
            get_button('player_up2') or \
            get_button('player_up3') or \
            get_button('player_up4') or \
            get_button('player_up5')
        if not UP and self.player.foot:
            self.jump_click = False
            self.jump_clicked = False
            self.jetpack_click = False
            self.jetpack_sound.stop()
        elif not UP and self.jetpack_click:
            self.jetpack_sound.stop()
            self.jetpack_click = False
        elif not UP and self.jump_click:
            self.jump_clicked = True
        
        
        DOWN = get_button('player_down') or \
            get_button('player_down2') or \
            get_button('player_down3')
        
        SHOOT = get_button('player_shoot') or \
            get_button('player_shoot2') or \
            get_button('player_shoot3')
        
        SWITCH = get_button('player_switch') or \
            get_button('player_switch2')
        
        if SWITCH:
            set_value('meta', True)
            if self.metatouch:
                self.death_handle()
            if get_current_music() != 1 and get_playlist_length() == 2:
                play_sound(self.switch_sound)
                set_music_index(1, get_music_pos())
                get_level().bg_color = (0,0,0)
        else:
            set_value('meta', False)
            
            if get_current_music() != 0:
                play_sound(self.switch_sound)
                set_music_index(0, get_music_pos())
                get_level().bg_color = (255,255,255)
        if SHOOT and not self.shoot_click:
            self.player.execute_event('on_shoot')
            self.shoot_click = 15
        elif SHOOT:
            self.shoot_click-=1
        elif not SHOOT:
            self.shoot_click = False
        vertical = UP-DOWN
        
        physics_events = get_physics_event()
        self.touch_wall = False
        for event in physics_events:
            #log(str(event.a)+" "+str(event.b)+" "+str(event.begin))
            if (event.a == 2 and event.b == 11) or (event.b == 2 and event.a == 11):
                self.player.foot = event.begin
            if (event.a == 4 and (15 > event.b >= 11 or 100<=event.b<=200)) or \
            (event.b == 4 and (100 <= event.a <= 200 or 15 > event.a >= 11)):

                self.touch_wall = event.begin
            if not get_value('meta') and ( ((event.a == 4 or event.a == 3) and (201<=event.b<=300 or 2001<=event.b<=3001)) or \
            ((event.b == 4 or event.b == 3) and (2001 <= event.a <= 3001 or 201 <= event.a <= 300))):
                self.touch_wall = event.begin
            if not get_value('meta') and ( (event.a == 3 and 20 >= event.b >= 15) or (event.b == 3 and 20 >= event.a >= 15)):
                self.touch_up = event.begin
            if get_value('meta') and ( (event.a == 3 and 30 >= event.b >= 25) or (event.b == 3 and 30 >= event.a >= 25)):
                self.touch_up = event.begin
            if (30 >= event.a >= 21 and event.b == 4) or (30 >= event.b >= 21 and event.a == 4):
                self.metatouch = event.begin
        if self.touch_wall:
            self.death_handle()
        if self.touch_up and not (DOWN and self.player.foot) :
            self.death_handle()
        if self.player.foot:
            self.jetpack_once = False
            if vertical == 1:
                if not self.jump_click:
                    self.jump_step = jump_step
                    self.jump_click = True
                    self.jump_clicked = False
                    self.last_index=1
                    if not SHOOT:
                        self.state = 'jump_begin'
                    else:
                        self.state = 'jump_shoot_begin'
                    self.anim_counter = self.anim_speed
                    self.loop = False
                else:
                    if not SHOOT:
                        if (not self.state == 'jump_begin' and not self.state == 'jump_shoot_begin') or self.last_index == 1:
                            if self.in_air:
                                self.last_index = 1
                                self.state = 'jump_end'
                                self.anim_counter = self.anim_speed
                                self.in_air = False
                            else:
                                self.state = 'move_right'
                    else:
                        if (not self.state == 'jump_begin' and not self.state == 'jump_shoot_begin') or self.last_index == 1:
                            if self.in_air:
                                self.last_index = 1
                                self.state = 'jump_shoot_end'
                                self.anim_counter = self.anim_speed
                                self.in_air = False
                            else:
                                self.move_horizontal(horizontal)
                                self.state = 'move_shoot'
                                  
            
            elif vertical == -1:
                if not SHOOT:
                    self.state = 'slide_right'
                else:
                    self.state = 'slide_shoot'
            else:
                if not SHOOT:
                    if self.in_air:
                        self.state = 'jump_end'
                        self.anim_counter = self.anim_speed
                        self.in_air = False
                    else:
                        self.move_horizontal(horizontal)
                        self.state = 'move_right'
                else:
                    if self.in_air:
                        self.state = 'jump_shoot_end'
                        self.anim_counter = self.anim_speed
                        self.in_air = False
                    else:
                        self.move_horizontal(horizontal)
                        self.state = 'move_shoot'
        else:
            self.in_air = True
            if not SHOOT:
                if not(self.last_index == 1 and self.anim_counter == self.anim_speed) \
                    and (self.state == 'jump_begin' or self.state == 'jump_shoot_begin'):
                    self.state = 'jump_begin'
                else:
                    self.state = 'jump_right'
            else:
                if not(self.last_index == 1 and self.anim_counter == self.anim_speed) \
                    and (self.state == 'jump_begin' or self.state == 'jump_shoot_begin'):
                    self.state = 'jump_shoot_begin'
                else:
                    self.state = 'jump_shoot_right'
            if vertical == 1:
                if (self.jump_clicked or not self.jump_click) and not self.jetpack_click:
                    #self.jetpackcool = jetpack_cool_const
                    #set_value("jet",self.jetpackcool)
                    play_sound(self.jetpack_sound,loop=-1)
                    self.jetpack_click = True
                if (not self.jetpack_once and self.jetpack_click and self.jetpackcool > 0) or \
                    (self.jetpack_once and self.jetpack_click and self.jetpackcool > 10):
                    
                    if not SHOOT:
                        self.state = 'jetpack'
                    else:
                        self.state = 'jetpack_shoot'
                    move(self.player.body,vy=-self.jet_velocity)
                    self.jetpackcool-=1
                    if self.jetpackcool < 0:
                        self.jetpack_sound.stop()
                        self.jetpack_once = True
                    set_value("jet",self.jetpackcool)
        
        if not self.jetpack_click and self.jetpackcool < jetpack_cool_const:
            self.jetpack_sound.stop()
            self.jetpackcool+=0.25
            set_value("jet",self.jetpackcool)
        
        if self.jump_step > 0:
            move(self.player.body,vy=-self.jump_velocity)
            self.jump_step -= 1
        self.update_pos()
    def update_pos(self):
        '''
        TODO: Should be in physics module engine
        ****************************************
        '''
        physics_pos = (0,0)
        if not pookoo:
            physics_pos = self.player.body.position
        else:
            physics_pos = physics.body_get_position(self.body)
        '''
        ****************************************
        '''
        physics_pos = (meter2pixel(physics_pos[0]),meter2pixel(physics_pos[1]))
        pos = (physics_pos[0]-self.player.screen_relative_pos[0]*get_screen_size()[0],
               physics_pos[1]-self.player.screen_relative_pos[1]*get_screen_size()[1])
        self.player.pos = pos