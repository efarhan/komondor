'''
Manages physics with Box2D
convert automatically from pixel to meters
'''
import math
from engine.const import move_speed, framerate,jump_step,gravity, log,\
    pybox2d, pookoo, jump_velocity
from event.physics_event import clear_physics_event, PhysicsEvent,\
    add_physics_event
from engine.image_manager import draw_rect
from engine.rect import Rect

if pookoo:
    import physics
else:
    from Box2D import *

ratio = 64/1.5


def pixel2meter(pixels):
    return pixels/ratio
def meter2pixel(meter):
    return meter*ratio
def set_ratio_pixel(new_ratio):
    ratio = new_ratio
    



timeStep = 1.0 / framerate
vel_iters, pos_iters = 10,10
index = 1
world = None

bullets = {}
bullet_index = 1000

def remove_bullet(index):
    global bullets,world
    #world.DestroyBody(bullets[index])
    try:
        world.DestroyBody(bullets[index])
        del bullets[index]
    except KeyError:
        pass
def get_bullet_pos(index):
    global bullets
    pos = bullets[index].position
    return (meter2pixel(pos[0]),meter2pixel(pos[1]))

def get_bullet_body(index):
    global bullets
    return bullets[index]

def fire_bullet(pos, size,velocity,index_delta=0):
    global bullets, bullet_index
    center_pos = (pos[0]+size[0]/2,pos[1]+size[1]/2)
    bullet = add_dynamic_object(center_pos)
    bullet.linearVelocity = ((velocity[0],velocity[1]))
    index = bullet_index+index_delta
    add_static_box((0,0), (size[0]/2,size[1]/2), 0, index, True, bullet)
    
    
    bullets[index] = bullet
    bullet_index+=1
    if bullet_index >= 2000:
        bullet_index = 1000

    return index
def deinit_physics():
    global world
    if pookoo:
        physics.close(world)
    else:
        del world
        world = None
def init_physics(gravity_arg=None):
    global world
    if world != None:
        deinit_physics()
    
    gravity_value = 0
    if(gravity_arg == None):
        gravity_value = gravity 
    else:
        gravity_value = gravity_arg
    if pookoo:
        world = physics.open(gravity_value)
    else:
        world = b2World(gravity=(0,gravity_value))
        world.contactListener = KuduContactListener()

def add_dynamic_object(pos):
    position = (pixel2meter(pos[0]),pixel2meter(pos[1]))
    dynamic_object = None
    if not pookoo:
        dynamic_object = world.CreateDynamicBody(position=position)
        dynamic_object.angle = 0
        dynamic_object.fixedRotation = 1
    else:
        dynamic_object = physics.body_add_dynamic(world,position[0],position[1])
    return dynamic_object

def add_static_object(pos):
    position = (pixel2meter(pos[0]),pixel2meter(pos[1]))
    static_object = None
    if not pookoo:
        static_object = world.CreateStaticBody(position=position)
        static_object.angle = 0
        static_object.fixedRotation = 1
    else:
        static_object = physics.body_add_dynamic(world,position[0],position[1])
    return static_object

def add_kinematic_object(pos):
    position = (pixel2meter(pos[0]),pixel2meter(pos[1]))
    kinematic_object = None
    if not pookoo:
        kinematic_object = world.CreateKinematicBody(position=position)
        kinematic_object.angle = 0
        kinematic_object.fixedRotation = 1
    else:
        kinematic_object = physics.body_add_dynamic(world,position[0],position[1])
    return kinematic_object

def remove_body(index):
    try:
        world.DestroyBody(index)
    except KeyError:
        pass
def update_physics():
    clear_physics_event()
    if not pookoo:
        world.Step(timeStep,vel_iters,pos_iters)
        world.ClearForces()
    else:
        from engine.loop import get_fps
        physics.step(world,get_fps())
    
def move(body,vx=None,vy=None):
    dyn_obj = body
    if not pookoo:
        velx,vely = dyn_obj.linearVelocity.x,dyn_obj.linearVelocity.y
        fx,fy=0,0
        if(vx != None):
            velx = vx * move_speed - velx
            fx = dyn_obj.mass * velx / timeStep
        if(vy != None):
            vely = vy * move_speed - vely
            fy = dyn_obj.mass * vely / timeStep
        dyn_obj.ApplyForce(b2Vec2(fx,fy),dyn_obj.worldCenter,True)
    
def add_static_box(pos,size,angle=0,data=0,sensor=False,body=None):
    static_body = body
    if(static_body == None):
        pos_body = (pixel2meter(pos[0]), pixel2meter(pos[1]))
        if pookoo:
            static_body = physics.body_add_static(world,pos[0],pos[1])
            return static_body
        else:
            static_body = world.CreateStaticBody(position=pos_body)
            static_body.angle = 0
    
    center_pos = (0,0)
    if body != None:
        center_pos = (pixel2meter(pos[0]),pixel2meter(pos[1]))
    if not pookoo:
        polygon_shape = b2PolygonShape()
        polygon_shape.SetAsBox(pixel2meter(size[0]), pixel2meter(size[1]),
                                   b2Vec2(center_pos),angle)
        fixture_def = b2FixtureDef()
        fixture_def.density = 0.5
        fixture_def.shape = polygon_shape
        fixture_def.userData = data
        fixture_def.isSensor = sensor
        static_body.CreateFixture(fixture_def)
        
        if body == None:
            return static_body
    else:
        physics.geometry_add_box(static_body, 
                                 center_pos[0], center_pos[1],
                                 pixel2meter(size[0]),pixel2meter(size[1]),
                                 angle, sensor,
                                 data)
        if body == None:
            return static_body
            
    

def add_static_circle(pos,radius,sensor=False,user_data=0):
    static_body = world.CreateStaticBody(\
                                position=(pixel2meter(pos[0]), pixel2meter(pos[1])),\
                                shapes=b2.Circle(radius=pixel2meter(radius),)\
                                                     )
    static_objects[index] = static_body
    index+=1
    return index - 1

if not pookoo:
    class KuduContactListener(b2ContactListener):
        def BeginContact(self, contact):
            a = contact.fixtureA.userData
            b = contact.fixtureB.userData
            add_physics_event(PhysicsEvent(a,b,True))
        def EndContact(self, contact):
            a = contact.fixtureA.userData
            b = contact.fixtureB.userData
            add_physics_event(PhysicsEvent(a,b,False))

def show_fixtures(screen,screen_pos,body):
    body_pos = body.position
    body_pos = (meter2pixel(body_pos[0]), meter2pixel(body_pos[1]))
    
    for fixture in body.fixtures:
        fixture_pos = fixture.shape.vertices[0]
        fixture_pos = (meter2pixel(fixture_pos[0]),meter2pixel(fixture_pos[1]))
        fixture_pos = (body_pos[0]+fixture_pos[0],body_pos[1]+fixture_pos[1])
        fixture_size = [0.0,0.0]
        fixture_size = [fixture.shape.vertices[1][0]-fixture.shape.vertices[0][0],fixture.shape.vertices[2][1]-fixture.shape.vertices[0][1]]
        fixture_size = (fixture_size[0]/2, fixture_size[1]/2)
        fixture_size = (meter2pixel(fixture_size[0]),meter2pixel(fixture_size[1]))
        fixture_pos = (fixture_pos[0]-fixture_size[0],fixture_pos[1]-fixture_size[1])
        fixture_size = (2*fixture_size[0],2*fixture_size[1])
        rect = Rect(fixture_pos,fixture_size)
        rect.set_center(fixture_pos)

        color = (255,0,0,100)
        if fixture.sensor == 1:
            color = (0,0,255,100)
        draw_rect(screen, screen_pos, rect, color)
