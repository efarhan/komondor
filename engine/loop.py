﻿'''
Main loop of the engine
'''

from engine.const import framerate, log, pookoo, startup
import sys
from levels.credits import Credits
if not pookoo:
	import pygame
else:
	import draw
	import window
import engine.init as init
import engine.level_manager as level_manager
from event.event_util import  update_event
from event.event_util import get_button, add_button
from levels.logo_kwakwa import Kwakwa
from engine.pyconsole import Console
from levels.gamestate import GameState

finish = False
screen = None
console = None
fps_clock = None

def get_console():
	global console
	return console

def set_screen(new_screen):
	global screen
	screen=new_screen
def get_screen():
	global screen
	return screen

def set_finish():
	global finish
	finish = True
def loop():
	global finish,screen,console,fps_clock
	if not pookoo:
		fps_clock = pygame.time.Clock()
		console = Console(screen, (0,0,init.get_screen_size()[0],init.get_screen_size()[1]/3))
	
	add_button('quit','ESC')
	add_button('toggle', 'f')
	if startup == 'logo_kwakwa':
		level_manager.switch_level(Kwakwa())
	elif startup == 'credits':
		level_manager.switch_level(Credits())
	else:
		level_manager.switch_level(GameState(startup))
	state = None
	if pookoo:
		state = draw.state_new()
	while not finish:
		if not pookoo:
			console.process_input()
		else:
			window.step()
			draw.clear()
			draw.rgb(0.0, 0.0, 0.0)
			draw.rectangle(window.width(), window.height())
		update_event()
		if not finish:
			finish = get_button('quit')
		f = level_manager.function_level()
		if f == 0:
			break
		else:
			if not pookoo:
				f(screen)
			else:
				f(state)
		
		if get_button('toggle'):
			init.toogle_fullscreen()
		
		if not pookoo:
			console.draw()
			pygame.display.flip()
			fps_clock.tick(framerate)
	if not pookoo:
		pygame.quit()
	else:
		draw.state_free(state)
		window.finish()

def start():
	global fps,screen
	if not pookoo:
		pygame.mixer.pre_init(44100, -16, 2, 2048)
		pygame.init()
	screen = init.init_screen()
	loop()
def get_fps():
	global fps_clock
	if not pookoo:
		return fps_clock.get_fps()
	
	
