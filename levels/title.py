'''
Created on 8 mars 2014

@author: efarhan
'''

from levels.scene import Scene
from engine.init import get_screen_size
from engine.const import framerate
from engine.image_manager import fill_surface
from game_object.image import Image
from engine.sound_manager import play_music, check_music_status, set_playlist
from levels.gamestate import GameState
from engine.font_manager import load_font
from game_object.text import Text
#font_obj, msg, sound_obj

class TitleScreen(Scene):
    def init(self):
        self.font = 'data/font/BLADRMF_.TTF'
        self.size = 300
        self.text = 'striders'
        self.text = Text((0,0), self.size, self.font, self.text, color=(255,255,255))
        self.text.pos = (get_screen_size()[0]/2-self.text.size[0]/2,get_screen_size()[1]/2-self.text.size[1]/2)
        set_playlist(['data/sound/vibration1.ogg'])

        
    def loop(self, screen):
        fill_surface(screen,0,0,0)
        self.text.loop(screen,(0,0))
        if(not check_music_status()):
            from engine.level_manager import switch_level
            switch_level(GameState('data/json/intro.json'))

