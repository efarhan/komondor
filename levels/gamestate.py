'''
Created on 9 dec. 2013

@author: efarhan
'''

from levels.scene import Scene
from engine.const import log, debug
from json_export.level_json import load_level
from engine.physics import init_physics, update_physics,deinit_physics


from engine.image_manager import fill_surface
from levels.gui import GUI
from event.mouse_event import show_mouse, get_mouse

from engine.sound_manager import update_music_status
from engine.stat import get_value, set_value
from event.event_util import add_button, get_button
from game_object.text import Text

class GameState(Scene,GUI):
    def __init__(self,filename):
        self.filename = filename
        self.event = {}
    def init(self):
        self.checkpoints = []
        init_physics()
        self.images = [
                       [],
                       [],
                       [],
                       [],
                       [],]
        self.physic_objects = [
                                ]
        self.screen_pos = (0,0)
        self.show_mouse = False
        if self.filename != "":
            log("Loading level "+self.filename)
            if not load_level(self):
                from engine.level_manager import switch_level
                switch_level(Scene())
        
        try:
            self.event['on_init'].execute()
        except KeyError:
            pass

        
        self.click = False
        
        add_button('pause','SPACE')
        add_button('pause2','JOY_BUTTON_7')
        add_button('reload','r')
        add_button('next_frame', 'RIGHT')
        self.pause = False
        self.pauseclick = False
        self.reloadclick = False
        self.nextframe_click = False
        
        
    def reload(self,newfilename):
        log('Reload')
        self.filename = newfilename
        player_pos = self.screen_pos
        
        self.init()
        '''Set player_pos to last checkpoints'''
        new_pos = [0,0]
        
        for pos in self.checkpoints:
            if pos[0] < player_pos[0] and pos[0] > new_pos[0]:
                new_pos = pos
        
        self.player.set_position(new_pos)
    def loop(self, screen):
        update_music_status()
        
        if (get_button('reload') or get_button('reset2')) and not self.reloadclick:
            self.reload(self.filename)
            self.reloadclick = True
            return
        elif not (get_button('reload') or get_button('reset2')):
            self.reloadclick = False
        
        '''Event
        If mouse_click on element, execute its event, of not null'''
        if self.show_mouse:
            show_mouse()
            mouse_pos, pressed = get_mouse()
            if pressed[0] and not self.click:
                event = None
                self.click = True
                for layer in self.images:
                    for image in layer:
                        if image.check_click(mouse_pos,self.screen_pos):
                            event = image.event
                if event:
                    event.execute()
            elif not pressed[0]:
                self.click = False
        
        if get_button('pause') or get_button('pause2'):
            if not self.pauseclick:
                self.pauseclick = True
                self.pause = not self.pause
        else:
            self.pauseclick = False
        if self.pause:
            if get_button('next_frame') and not self.nextframe_click:
                self.nextframe = True
                self.nextframe_click = True
            elif not get_button('next_frame'):
                self.nextframe_click = False
        if not self.pause or self.nextframe:
            set_value('x_pos',self.screen_pos[0])
            if self.player.foot:
                set_value('y_pos',self.screen_pos[1])
            try:
                self.event['on_loop'].execute()
            except KeyError:
                pass

            fill_surface(screen, self.bg_color[0],self.bg_color[1],self.bg_color[2],255)
        
            update_physics()
            
            
            '''Show images'''
            for i in range(self.player.layer):
                remove_image=[]
                for j in range(len(self.images[i])):
                    if get_value('meta') == self.images[i][j].get_meta():
                        self.images[i][j].loop(screen,self.screen_pos)
                    if self.images[i][j].remove:
                        remove_image.append(self.images[i][j])
                for r in remove_image:
                    self.images[i].remove(r)
            self.screen_pos = self.player.loop(screen,self.screen_pos)
            for i in range(self.player.layer,len(self.images)):
                remove_image=[]
                for j in range(len(self.images[i])):
                    if get_value('meta') == self.images[i][j].get_meta():
                        self.images[i][j].loop(screen,self.screen_pos)
                    if self.images[i][j].remove:
                        remove_image.append(self.images[i][j])
                for r in remove_image:
                    self.images[i].remove(r)
            GUI.loop(self,screen)
            
            for physic_object in self.physic_objects:
                physic_object.loop(screen,self.screen_pos)
            
            self.nextframe = False
            
    def exit(self, screen):
        deinit_physics()
        Scene.exit(self, screen)
