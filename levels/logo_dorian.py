from levels.scene import Scene
from engine.init import get_screen_size
from engine.const import framerate
from engine.image_manager import fill_surface
from game_object.image import Image
from engine.sound_manager import play_music, check_music_status, set_playlist
from levels.gamestate import GameState
from levels.title import TitleScreen
#font_obj, msg, sound_obj

class Dorian(Scene):
	def init(self):
		self.text = Image('data/sprites/text/logo-dorian_sred.png', 
						(get_screen_size()[0]/2,get_screen_size()[1]/2))
		set_playlist(['data/sound/logo_dorian.ogg'])
		
	def loop(self, screen):
		fill_surface(screen,255, 255, 255)
		self.text.pos = (get_screen_size()[0]/2-self.text.size[0]/2, 
						get_screen_size()[1]/2-self.text.size[1]/2)
		self.text.loop(screen, (0,0))

		if(not check_music_status()):
			from engine.level_manager import switch_level
			switch_level(TitleScreen())


			
