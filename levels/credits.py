'''
Created on 8 mars 2014

@author: efarhan
'''
from levels.scene import Scene
from engine.init import get_screen_size
from engine.const import framerate
from engine.image_manager import fill_surface
from game_object.image import Image
from engine.sound_manager import play_music, check_music_status, set_playlist
from levels.gamestate import GameState
from engine.font_manager import load_font
from game_object.text import Text
#font_obj, msg, sound_obj

class Credits(Scene):
    def init(self):
        self.font = 'data/font/BLADRMF_.TTF'
        self.size = 100
        self.texts = [
            "striders",
            "a game by team kwakwa",
            "",
            "lead designer",
            "elias farhan",
            "",
            "game design",
            "daniel cho",
            "",
            "lead art",
            "dan iorgulescu",
            "",
            "artists",
            "val terramorsi",
            "virgile paultre",
            "nahoy07200",
            "",
            "music & sfx",
            "dorian sred"
                    
        ]
        for i in range(len(self.texts)):
            self.texts[i] = Text((0,0), self.size, self.font, self.texts[i], color=(255,255,255))
            self.texts[i].pos = (get_screen_size()[0]/2-self.texts[i].size[0]/2,self.texts[i].pos[1]+i*self.size)
        set_playlist(['data/music/theme1.ogg'])
        self.count = -get_screen_size()[1]
        
    def loop(self, screen):
        fill_surface(screen,0,0,0)
        for i in range(len(self.texts)):
            self.texts[i].loop(screen,(0,self.count))
        self.count += 1
        if(self.count >= self.size*len(self.texts)):
            from engine.level_manager import switch_level
            switch_level(0)


            
