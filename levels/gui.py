'''
Created on Feb 26, 2014

@author: efarhan
'''

from engine.init import get_screen_size
from game_object.text import Text
from game_object.image import Image
from event.mouse_event import show_mouse, get_mouse
from json_export.gui_json import load_gui
from engine.rect import Rect
from engine.stat import get_value
from animation.runner_shooter import jetpack_cool_const
from engine.image_manager import draw_rect


class GUI():
    def __init__(self,filename):
        self.tutorials_text = []
        self.text_pos = (0,0)
        self.text_size = 50
        self.tutorial_color = (0,255,0)
        self.font = 'data/font/BLADRMF_.TTF'
        load_gui(self,filename)
        self.show_mouse = True
        self.jet_stat = Rect((0,0), (100,10))
    def loop(self,screen):
        for tutorials in self.tutorials_text:
            tutorials.loop(screen, self.screen_pos, self.player.pos)
        x_size = int(float(get_value('jet'))/jetpack_cool_const*100)
        if x_size > 0:
            self.jet_stat.size = (x_size,10)
            self.jet_stat.update_rect()
            draw_rect(screen, (0,0), self.jet_stat, (0,255,0,255))
        if get_value('boss_life'):
            self.boss_stat = Rect((get_screen_size()[0]-100,0), (100,10))
            self.boss_stat.size = (get_value('boss_life')/50.0*100,10)
            self.boss_stat.update_rect()
            draw_rect(screen, (0,0), self.boss_stat, (255,0,0,255))
            