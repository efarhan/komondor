'''
Created on 8 sept. 2013

@author: efarhan
'''
from game_object.image import Image, AnimImage
from json_export.player_json import load_player
from engine.const import log,pookoo, debug

from animation.player_animation import PlayerAnimation
from game_object.game_object_util import GameObject
from engine.physics import show_fixtures, pixel2meter
from engine.init import get_screen_size
from engine.stat import get_value


class Player(AnimImage):
    def __init__(self, path, pos=(0,0),layer = 1):
        GameObject.__init__(self)
        self.layer = layer
        self.filename = path
        self.anim = PlayerAnimation(self)
        self.body = None
        self.foot = False
        self.speed = 1
        self.i = 0
        self.image_loop = False
        self.center_image = True
        self.screen_pos_factor = 1.0
        log('Loading player file '+self.filename)
        status = load_player(self)
        if status != 1:
            log("Error while loading player "+str(status))
            return
        self.init_image()
    def init_image(self):
        self.anim.load_images(size=self.size,permanent=True)
        self.meta_binary = AnimImage('data/sprites/hero/meta/', self.pos)
    def loop(self, screen, screen_pos):
        self.anim.update_state()
        
        AnimImage.loop(self, screen, screen_pos)
        self.meta_binary.pos = (self.pos[0]-self.size[0]/2,self.pos[1]-self.size[1])
        self.meta_binary.screen_relative_pos = self.screen_relative_pos
        if get_value('meta'):
            self.meta_binary.loop(screen, screen_pos)
        if debug:
            show_fixtures(screen, screen_pos, self.body)
        return self.pos
    def set_position(self,pos):
        self.pos = (pos[0]+self.screen_relative_pos[0]*get_screen_size()[0],
                    pos[1]+self.screen_relative_pos[1]*get_screen_size()[1])
        self.body.position = (pixel2meter(self.pos[0]),pixel2meter(self.pos[1]))
    def set_relative_pos(self,relative_pos):
        pos = (self.pos[0]-self.screen_relative_pos[0],self.pos[1]-self.screen_relative_pos[1])
        self.screen_relative_pos = relative_pos
        #self.pos = (self.pos[0]+self.screen_relative_pos[0]*get_screen_size()[0],
                    #self.pos[1]+self.screen_relative_pos[1]*get_screen_size()[1])
        #self.body.position = (pixel2meter(self.pos[0]),pixel2meter(self.pos[1]))
        
        