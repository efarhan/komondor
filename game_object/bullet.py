'''
Created on 1 mars 2014

@author: efarhan
'''
from game_object.game_object_util import GameObject
from engine.image_manager import draw_rect
from engine.physics import fire_bullet, get_bullet_pos, remove_bullet,\
    show_fixtures, get_bullet_body
from event.physics_event import get_physics_event
from engine.const import log, debug
from engine.init import get_screen_size
from game_object.image import AnimImage
from engine.level_manager import get_level
from engine.stat import get_value


class Bullet(AnimImage):
    def __init__(self,pos,size,velocity,path,index,color=(0,0,0,255),angle=0,meta = False):
        
        AnimImage.__init__(self, path, pos, size=size)
        '''TODO: This is a horrible hack to change'''
        self.angle = angle
        if angle == 0:
            self.anim.state_range = {"begin": [0,2],"loop":[2,6]}
        self.meta = meta
        self.begin = True
        self.color = color
        self.pos = pos
        self.size = size
        self.update_rect()
        self.velocity = velocity
        self.anim.anim_speed = 1
        self.index = fire_bullet(pos, self.size,velocity,index)
    def loop(self,screen,screen_pos):
        '''Get physical event and remove if touch anything'''
        physical_event = get_physics_event()
        for e in physical_event:
            if not self.meta:
                if (e.a == self.index and (20 >= e.b >= 10)) or ((20 >= e.a >= 10 ) and e.b == self.index):
    
                    from event.run_shoot_event import ExplosionEvent
                    ExplosionEvent(get_level(), 'arg',pos=self.pos).execute()
                    self.remove = True
                    remove_bullet(self.index)
                    return
            else:
                if (e.a == self.index and (30 >= e.b >= 20)) or ((30 >= e.a >= 20 ) and e.b == self.index):
    
                    from event.run_shoot_event import ExplosionEvent
                    ExplosionEvent(get_level(), 'arg',pos=self.pos).execute()
                    self.remove = True
                    remove_bullet(self.index)
                    return
            if 1000<=self.index<=2000 or self.index >= 10000:
                if (e.a == self.index and (100<= e.b<=200)) or (( 100 <= e.a <=200) and e.b == self.index):
                    from event.run_shoot_event import ExplosionEvent
                    ExplosionEvent(get_level(), 'arg',pos=self.pos).execute()
                    self.remove = True
                    
                    remove_bullet(self.index)
                    return
            
            else:
                if (e.a == self.index and (0<= e.b<=10)) or (( 0 <= e.a <=10) and e.b == self.index):
                    from event.run_shoot_event import ExplosionEvent
                    ExplosionEvent(get_level(), 'arg',pos=self.pos).execute()
                    self.remove = True
                    remove_bullet(self.index)
                    return
        if self.remove:
            remove_bullet(self.index)
            return
        if get_value('x_pos')-2*get_screen_size()[0] > self.pos[0] > get_value('x_pos')+2*get_screen_size()[0]:
            self.remove = True
        self.pos = get_bullet_pos(self.index)
        self.pos = (self.pos[0]-self.size[0]/2,self.pos[1]-self.size[1]/2)
        self.update_rect()
        if self.begin and self.anim.last_index == 1:
            self.begin = False
            self.anim.state = 'loop'
        elif self.begin:
            self.anim.state = 'begin'
        AnimImage.loop(self, screen, screen_pos)
        if debug:
            show_fixtures(screen, screen_pos, get_bullet_body(self.index))