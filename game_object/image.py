'''
Created on 11 sept. 2013

@author: efarhan
'''

from engine.image_manager import  show_image, load_image, \
    load_image_with_size, get_image_size
from animation.animation_util import Animation
from engine.init import get_screen_size
from engine.rect import Rect
from game_object.game_object_util import GameObject
from engine.const import pookoo,log
from engine.stat import get_value
if pookoo:
    import texture

class Image(GameObject):
    def __init__(self,path,pos,screen_relative_pos=None,size=None,angle=0):
        GameObject.__init__(self)
        self.img = 0
        self.angle = 0
        self.pos = pos
        self.path = path
        self.size = size
        self.image_loop = False
        self.i = 0
        self.screen_relative_pos = None
        self.screen_pos_factor = 1.0
        self.center_image = False
        self.init_image()
    def init_image(self):
        if self.size == None:
            self.img = load_image(self.path)
            self.size = get_image_size(self.img)
        else:
            self.img = load_image_with_size(self.path, self.size)
        self.rect = Rect(self.pos, self.size)
    def loop(self, screen, screen_pos):
        pos = self.pos
        try:
            if self.screen_relative_pos != None: 
                pos = (pos[0]+self.screen_relative_pos[0]*get_screen_size()[0],
                       pos[1]+self.screen_relative_pos[1]*get_screen_size()[1])
        except AttributeError:
            pass
        factor = 1
        if pookoo:
            
            factor = self.size[0]/texture.size(self.img)[0]
            log(str(texture.size(self.img))+" "+str(self.size))
        if self.show:
            if self.image_loop and get_value('x_pos')>(self.i+1)*self.size[0]/self.screen_pos_factor:
                self.i+=1
            show_image(self.img, screen, (pos[0]-self.screen_pos_factor*screen_pos[0]+self.i*self.size[0],pos[1]-self.screen_pos_factor*screen_pos[1]),angle=self.angle,factor=factor,center_image=self.center_image)
            if self.image_loop:
                
                show_image(self.img, screen, (pos[0]-self.screen_pos_factor*screen_pos[0]+(self.i+1)*self.size[0],pos[1]-self.screen_pos_factor*screen_pos[1]),angle=self.angle,factor=factor,center_image=self.center_image)
        
class AnimImage(Image):
    '''Can be animated if a directory is given,
    if a png file is given, it will load it
    to load several file, like player do not call this constructor'''
    def __init__(self,path,pos,size=None,angle=0):
        self.anim = Animation()
        Image.__init__(self, path, pos, screen_relative_pos=None, size=size)

    def init_image(self):
        '''init only one directory or one image,
        for several directories, please set manually
        animation class before and call directly anim.load_images'''
        self.anim.path_list = [self.path]
        self.anim.load_images(self.size)
        if self.size == None:
            self.size = self.anim.size
        self.rect = Rect(self.pos, self.size)
    def loop(self,screen,screen_pos):
        
        self.anim.update_animation()
        self.img = self.anim.img
        Image.loop(self, screen, screen_pos)
        
class TriImage(Image):
    def __init__(self, path, pos, screen_relative_pos=None, size=None, angle=0):
        Image.__init__(self, path, pos, screen_relative_pos=screen_relative_pos, size=size, angle=angle)
    def init_image(self):
        Image.init_image(self)
    def loop(self, screen, screen_pos):
        Image.loop(self, screen, screen_pos)


