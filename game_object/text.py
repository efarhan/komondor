'''
Created on Feb 19, 2014

@author: efarhan
'''
from game_object.game_object_util import GameObject
from engine.font_manager import load_font, load_text
from engine.rect import Rect
from engine.const import log
from engine.init import get_screen_size

class Text(GameObject):
    def __init__(self,pos,size,font,text,color=(0,0,0),gradient=0):
        GameObject.__init__(self)
        self.pos = pos
        self.color = color
        self.font = load_font(font,size)
        self.set_text(text)
        self.gradient = gradient
        self.time = 1
    def set_text(self,text):
        self.text = text
        self.time = 1
        self.change_text(text)
    def change_text(self,text,color=None):
        
        new_color = color
        if color == None:
            new_color = self.color
        self.text_surface = load_text(self.font,text,new_color)
        if self.text_surface:
            self.size = self.text_surface.get_size()
            self.update_rect()
        
    def loop(self,screen,screen_pos):
        if self.time < self.gradient:
            self.time += 1
            self.change_text(self.text[0:int(self.time/self.gradient*len(self.text))])
        screen.blit(self.text_surface,(self.pos[0]-screen_pos[0],self.pos[1]-screen_pos[1]))

class TutorialsText(Text):
    def __init__(self, pos, size, font, text, color=(0, 255, 0), gradient=0,x_from=0, x_to=0):
        Text.__init__(self, pos, size, font, text, color=color, gradient=gradient)
        self.x_from = x_from
        self.x_to = x_to
        self.pos = (get_screen_size()[0]/2-self.text_surface.get_size()[0]/2,pos[1])
    def loop(self, screen, screen_pos,player_pos):
        if self.x_from < player_pos[0] < self.x_to:
            Text.loop(self, screen, (0,0))