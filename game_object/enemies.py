'''
Created on Mar 7, 2014

@author: efarhan
'''
import math
import time
from game_object.game_object_util import GameObject
from engine.level_manager import get_level, switch_level
from engine.init import get_screen_size
from game_object.image import Image, AnimImage
from engine.const import log, debug
from game_object.physic_object import AngleSquare
from engine.physics import add_static_box, remove_body, show_fixtures,\
    pixel2meter, add_static_object

from event.physics_event import get_physics_event
from engine.stat import get_value, set_value
from engine.image_manager import load_image
import random
from engine.sound_manager import load_sound, play_sound






index = 100

class Drone(Image):
    def __init__(self, y_pos, velocity, shoot_frequency, path='data/sprites/drones/drone.png', screen_relative_pos=None, size=None, angle=0):
        global index
        self.gamestate = get_level()
        pos = (self.gamestate.screen_pos[0]+get_screen_size()[0],get_value('y_pos')+y_pos)
        self.velocity = velocity
        self.shoot_frequency =shoot_frequency
        self.counter = 0
        Image.__init__(self, path, pos, screen_relative_pos=screen_relative_pos, size=size, angle=angle)
        self.angle = 20
        self.index = 100+index
        self.body = add_static_box(self.rect.get_center(), self.size, angle, data=self.index, sensor=True)
        index=((index+1)%200)+101
    def get_meta(self):
        self.meta = get_value('meta')
        return GameObject.get_meta(self)
    def loop(self, screen, screen_pos):
        if self.pos[0]+self.velocity*self.counter<self.gamestate.screen_pos[0]-get_screen_size()[0]:
            remove_body(self.body)
            self.remove = True
            return
        if self.counter%self.shoot_frequency == 0:
            from event.run_shoot_event import FireBulletEvent
            FireBulletEvent(get_level(), (-10,10),size=(50,15), path='data/sprites/laser/', pos=(self.pos[0]+self.velocity*self.counter,self.pos[1]+self.size[1]),index=1001,angle=20).execute()
        physics_events = get_physics_event()
        for event in physics_events:
            if(event.a == self.index and 1000<=event.b<=2000) or (event.b == self.index and 1000<=event.a<=2000):
                self.remove = True
                from event.run_shoot_event import ExplosionEvent
                remove_body(self.body)
                ExplosionEvent(get_level(), pos_type='arg', pos=(self.pos[0]+self.velocity*self.counter,self.pos[1])).execute()
                return
        self.body.position = (pixel2meter(self.pos[0]+self.size[0]+self.velocity*self.counter),pixel2meter(self.pos[1]))
        if not self.meta:
            Image.loop(self, screen, (screen_pos[0]-self.velocity*self.counter,screen_pos[1]))
        if debug:
            show_fixtures(screen, screen_pos, self.body)
        self.counter += 1

class Strider(Image):

    def __init__(self, y_pos,amplitude,frequency,velocity,path='data/sprites/strider/strider.png', screen_relative_pos=None, size=None, angle=0,counter=0):
        global index
        self.frequency = frequency
        self.amplitude = amplitude
        self.counter = counter
        self.gamestate = get_level()
        self.velocity = velocity
        
        pos = (self.gamestate.screen_pos[0]+get_screen_size()[0],get_value('y_pos')+y_pos)
        Image.__init__(self, path, pos, screen_relative_pos=screen_relative_pos, size=size, angle=angle)
        self.imgs = [self.img]
        self.imgs.append(load_image('data/sprites/strider/metastrider.png'))
        self.index = index
        self.body = add_static_box(self.rect.get_center(), self.size, angle, data=index, sensor=True)
        index=((index+1)%200)+101
    def get_meta(self):
        self.meta = get_value('meta')
        return GameObject.get_meta(self)
    def loop(self,screen,screen_pos):
        if self.pos[0]+self.velocity*self.counter<self.gamestate.screen_pos[0]-get_screen_size()[0]:
            remove_body(self.body)
            self.remove = True
            return
        
        self.result = self.frequency/360.0*self.counter
        self.elevation = math.cos(self.result)
        self.angle = -math.sin(self.result)*10
        self.counter += 1
        y_pos = self.amplitude*self.elevation
        physics_events = get_physics_event()
        for event in physics_events:
            if(event.a == self.index and (1000<=event.b<=2000 or event.b >= 10000)) or (event.b == self.index and (1000<=event.a<=2000 or event.a >=10000)):
                self.remove = True
                from event.run_shoot_event import ExplosionEvent
                remove_body(self.body)
                if not self.meta:
                    ExplosionEvent(get_level(), pos_type='arg', pos=(self.pos[0]+self.velocity*self.counter,self.pos[1]+y_pos)).execute()
                else:
                    ExplosionEvent(get_level(), pos_type='arg', pos=(self.pos[0]+self.velocity*self.counter,self.pos[1]+y_pos),meta=True,path='data/sprites/metaexplosion/').execute()
                return
        self.body.position = (pixel2meter(self.pos[0]+self.size[0]+self.velocity*self.counter),pixel2meter(self.pos[1]+y_pos))
        self.img = self.imgs[self.meta]
        Image.loop(self, screen, (screen_pos[0]-self.velocity*self.counter,screen_pos[1]-y_pos))
        if debug:
            show_fixtures(screen, screen_pos, self.body)

        
class Boss(AnimImage):
    def __init__(self, y_pos,path='data/sprites/boss/', size=None, angle=0):
        self.gamestate = get_level()
        pos = (self.gamestate.screen_pos[0]+get_screen_size()[0],get_value('y_pos')+y_pos)
        self.PV = 50
        self.frequency = 120.0
        self.frequency_shoot = 60
        self.death = False
        self.death_counter = 0
        AnimImage.__init__(self, path, pos, size=size, angle=angle)
        self.anim.anim_speed = 1
        self.x_pos = 1.0
        self.counter = 0
        self.phase = 1
        self.y_pos = float(y_pos)/get_screen_size()[1]
        self.sound = load_sound('data/sound/helico1.ogg')
        play_sound(self.sound,loop=-1)
        self.body = add_static_object(self.pos)
        add_static_box((0,0), self.size, angle=0, data=50, sensor=True, body=self.body)
    def get_meta(self):
        self.meta = get_value('meta')
        return GameObject.get_meta(self)
    def loop(self, screen, screen_pos):
        set_value('boss_life',self.PV)
        for p in get_physics_event():
            if p.begin and (p.a == 50 and (1000<=p.b<=2000)) or (p.b == 50 and (1000<=p.a<=2000)):
                self.PV-=1
                from event.run_shoot_event import ExplosionEvent
                ExplosionEvent(get_level(), pos_type='arg', pos=(self.pos[0],self.pos[1])).execute()
       
            
        self.x_pos = math.cos(self.counter/self.frequency)/2+0.5
        if self.phase == 1 and self.x_pos > 0.95 and not self.death:
            from event.run_shoot_event import SpawnStriderEvent,SpawnDroneEvent
            from event.run_shoot_event import FireBulletEvent
            FireBulletEvent(self.gamestate, (0,-10), size=(50,50),path='data/sprites/boss_bullet/', pos=(self.pos[0]+self.size[0]/2,self.pos[1]+self.size[1]/2), index=1001).execute()
            FireBulletEvent(self.gamestate, (-5,-10), size=(50,50),path='data/sprites/boss_bullet/', pos=(self.pos[0]+self.size[0]/2,self.pos[1]+self.size[1]/2), index=1001).execute()
            FireBulletEvent(self.gamestate, (-10,-10), size=(50,50),path='data/sprites/boss_bullet/', pos=(self.pos[0]+self.size[0]/2,self.pos[1]+self.size[1]/2), index=1001).execute()
            SpawnStriderEvent(self.gamestate, nmb=10).execute()
            SpawnDroneEvent(self.gamestate, nmb=1).execute()
            self.phase = 2
        elif self.x_pos < 0.95 and not self.death:
            self.phase = 1
        self.angle = math.sin(self.counter/self.frequency)*10
        self.pos = (self.x_pos*get_screen_size()[0]+get_value('x_pos'),self.y_pos*get_screen_size()[1]+get_value('y_pos'))
        self.body.position = (pixel2meter(self.pos[0]),pixel2meter(self.pos[1]))
        if self.PV <= 0 and not self.death:
            self.death = True

            self.death_counter = 60*3

        if self.death and self.death_counter <= 0:
            self.remove = True
            self.sound.stop()
            self.gamestate.player.anim.jetpack_sound.stop()
            from levels.credits import Credits
            switch_level(Credits())
            return
        elif self.death:
            from event.run_shoot_event import ExplosionEvent
            ExplosionEvent(self.gamestate, pos_type='arg', pos=(self.pos[0]+self.size[0]-random.random()*self.size[0],self.pos[1]+self.size[1]-random.random()*self.size[1])).execute()
            self.y_pos+=0.005 
            self.death_counter -=1
        if not self.meta:
            AnimImage.loop(self, screen, screen_pos)
        self.counter += 1