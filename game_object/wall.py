'''
Created on 4 mars 2014

@author: efarhan
'''
from game_object.image import Image, AnimImage
from game_object.physic_object import AngleSquare
from engine.const import debug

class Wall(Image):
    #delta size ground -550
    def __init__(self, type, pos, screen_relative_pos=None, size=None, angle=0):
        path = ''
        if type == 'Wall':
            path = "data/sprites/obstacles/obstacle_crouch2.png"
        elif type == 'Barrier':
            path = 'data/sprites/obstacles/obstacle_fleshbarrier1.png'
        Image.__init__(self, path, pos, screen_relative_pos=screen_relative_pos, size=size, angle=angle)
        self.physic_object = AngleSquare((self.pos[0],self.pos[1]-100), 
                                         (self.size[0]/2,self.size[1]), data=15, sensor=True)
    def loop(self, screen, screen_pos):
        Image.loop(self, screen, screen_pos)
        if debug:
            self.physic_object.loop(screen, screen_pos)
class Firewall(Image):
    def __init__(self, path="data/sprites/obstacles/obstacle_firewallnormal1.png", pos=(0,0), screen_relative_pos=None, size=None, angle=0):
        Image.__init__(self, path, pos, screen_relative_pos=screen_relative_pos, size=size, angle=angle)
        self.meta = True
        self.physic_object = AngleSquare((self.pos[0],self.pos[1]-100), 
                                         (self.size[0]/2,self.size[1]), data=25, sensor=True)
    def loop(self, screen, screen_pos):
        Image.loop(self, screen, screen_pos)
        if debug:
            self.physic_object.loop(screen, screen_pos)
