'''
Created on 3 mars 2014

@author: efarhan
'''
from game_object.image import AnimImage

class Explosion(AnimImage):
    def __init__(self, path, pos, size=None):
        AnimImage.__init__(self, path, pos)
        self.anim.anim_speed=4
    def loop(self, screen, screen_pos):
        self.anim.loop = False
        if self.anim.img == self.anim.img_indexes[-1]:
            self.show = False
        AnimImage.loop(self, screen, screen_pos)