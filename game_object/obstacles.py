'''
Created on 4 mars 2014

@author: efarhan
'''
from game_object.image import Image
from game_object.physic_object import AngleSquare
from engine.const import debug, log
from engine.stat import get_value
from engine.image_manager import load_image

class Crouch(Image):
    #delta size ground object -150
    def __init__(self, pos, path='data/sprites/obstacles/obstacle_crouch1.png', screen_relative_pos=None, size=None, angle=0):
        Image.__init__(self, path, pos, screen_relative_pos=screen_relative_pos, size=size, angle=angle)
        #(32,17),(107,80)
        physic_pos = (self.pos[0]+32,self.pos[1]+80)
        physic_size = (107-32,80-17)
        self.physic_object = AngleSquare(physic_pos, physic_size, data=15, sensor=True)
    def loop(self, screen, screen_pos):
        Image.loop(self, screen, screen_pos)
        if debug:
            self.physic_object.loop(screen,screen_pos)
            
class JumpObstacle(Image):
    #delta size -150
    def __init__(self, image_type, pos):
        self.path_root = 'data/sprites/obstacles/'
        path = self.path_root+"obstacle_jump%i.png"%image_type
        Image.__init__(self, path, pos)
        self.imgs = [self.img]
        self.imgs.append(load_image(self.path_root+"obstacle_metavers_jump%i.png"%image_type))
        log(self.imgs)
        if image_type == 1:
            self.physic_object = AngleSquare(self.pos, self.size, data=11, sensor=False)
        elif image_type == 3:
            self.physic_object = AngleSquare((self.pos[0]+31,self.pos[1]+25), (155-31,self.size[1]-25), data=11, sensor=False)
    def get_meta(self):
        self.meta = get_value('meta')
        return Image.get_meta(self)
    def loop(self, screen, screen_pos):

        self.img = self.imgs[self.meta]
        Image.loop(self, screen, screen_pos)
        if debug:
            self.physic_object.loop(screen, screen_pos)