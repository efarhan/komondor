'''
Created on 2 mars 2014

@author: efarhan
'''
import random
from game_object.image import Image
from engine.image_manager import load_image, get_image_size, show_image
from game_object.physic_object import AngleSquare
from engine.const import debug

class Building(Image):
    def __init__(self, path, pos, nmb=1):
        self.nmb = nmb
        Image.__init__(self, path, pos)
        size = (nmb+2)*200
        self.physic_object = AngleSquare(self.pos, (size,200), data=11)
    def init_image(self):
        self.left = load_image(self.path+"left.png")
        self.right = load_image(self.path+"right.png")
        self.middle = load_image(self.path+"middle.png")
        self.middle_down = load_image(self.path+"middle_down.png")
        self.down_map = [random.randint(0,1) for i in range(self.nmb)]
    def loop(self, screen, screen_pos):
        self.img = self.left
        pos = (self.pos[0]-screen_pos[0], self.pos[1]-screen_pos[1])
        show_image(self.img, screen, pos)
        for i in range(self.nmb):
            if self.down_map[i]:
                self.img = self.middle_down
            else:
                self.img = self.middle
            pos = (self.pos[0]+(i+1)*get_image_size(self.img)[0]-screen_pos[0], self.pos[1]-screen_pos[1])
            show_image(self.img, screen, pos)
        self.img = self.right
        pos = (self.pos[0]+(self.nmb+1)*get_image_size(self.img)[0]-screen_pos[0], self.pos[1]-screen_pos[1])
        show_image(self.img, screen, pos)
        if debug:
            self.physic_object.loop(screen, screen_pos)
class MetaBuilding(Building):
    def init_image(self):
        self.imgs = [load_image(self.path+"bricknormal_meta_03.png"),
                     load_image(self.path+"bricknormal_meta_02.png"),
                     load_image(self.path+"bricknormal_meta_01.png")]
        self.map_brick = [self.imgs[random.randint(0,2)] for i in range(self.nmb+2)]
        self.meta = True
    def loop(self,screen,screen_pos):
        for i in range(self.nmb+2):
            self.img = self.imgs[0]
            pos = (self.pos[0]+(i)*get_image_size(self.img)[0]-screen_pos[0], self.pos[1]-screen_pos[1])
            show_image(self.map_brick[i], screen, pos)