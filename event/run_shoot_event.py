import random
from event.event_util import Event
from engine.init import get_screen_size
from game_object.bullet import Bullet
from game_object.explosion import Explosion
from engine.sound_manager import load_sound, play_sound
from game_object.enemies import Strider, Drone, Boss
from engine.stat import get_value
from engine.const import log

class FireBulletEvent(Event):
    def __init__(self,gamestate,
                 velocity=(50,0),
                 size=(30,3),
                 color=(0,0,0,255),
                 path='data/sprites/bullet/',
                 pos=None,
                 index=0,
                 angle=0):
        Event.__init__(self)
        self.gamestate = gamestate
        self.velocity = velocity
        self.size = size
        self.color = color
        self.path = path
        self.pos = pos
        self.index = index
        self.angle = angle
        self.meta = False
        
    def execute(self):
        index = self.index
        meta = self.meta
        path =self.path
        if get_value('meta') and self.index == 0:
            
            index = 10000
            path = 'data/sprites/metabullet/'
            meta = True
        pos = None
        if self.pos == None:
            player_pos = self.gamestate.player.pos
            player_rpos = self.gamestate.player.screen_relative_pos
            player_size = self.gamestate.player.size
            pos = (player_pos[0]+player_rpos[0]*get_screen_size()[0]+player_size[0]/2,
                   player_pos[1]+player_rpos[1]*get_screen_size()[1]-player_size[1]/2)
        else:
            pos = self.pos
        b = Bullet(pos, 
                                               self.size, 
                                               self.velocity, 
                                               path,
                                               index, 
                                               self.color,
                                               angle = self.angle)
        b.meta = meta
        self.gamestate.images[3].append(b)
        Event.execute(self)

class ExplosionEvent(Event):
    def __init__(self,gamestate, pos_type,
                 path='data/sprites/explosion/',pos = None,meta=False):
        Event.__init__(self)
        self.gamestate = gamestate
        self.pos_type = pos_type
        if self.pos_type == 'arg':
            self.pos = pos
        self.path = path
        self.meta =meta
        self.explosion_sounds = [
                                 load_sound('data/sound/explosion.ogg'),
                                 load_sound('data/sound/explosion2.ogg')
                                 ]
    def execute(self):
        
        if self.pos_type == 'player':
            player_pos = self.gamestate.player.pos
            player_rpos = self.gamestate.player.screen_relative_pos
            player_size = self.gamestate.player.size
            pos = (player_pos[0]+player_rpos[0]*get_screen_size()[0]-player_size[0]/2,
                   player_pos[1]+player_rpos[1]*get_screen_size()[1]-player_size[1]/2)
            self.pos = pos

            self.meta = get_value('meta')
            
            if self.meta:
                
                self.path = 'data/sprites/metaexplosion/'
        play_sound(self.explosion_sounds[self.meta])
        self.explosion = Explosion(self.path, self.pos)
        
        self.explosion.meta = self.meta
        self.gamestate.images[3].append(self.explosion)
        Event.execute(self)

class SpawnStriderEvent(Event):
    def __init__(self,gamestate,nmb=1):
        Event.__init__(self)
        self.gamestate = gamestate
        self.nmb = nmb
    def execute(self):
        amplitude = 100
        for i in range(self.nmb):
            y_pos = (random.random()%self.gamestate.player.screen_relative_pos[1])*get_screen_size()[1]
            if y_pos > self.gamestate.player.screen_relative_pos[1]*get_screen_size()[1]-amplitude:
                y_pos = self.gamestate.player.screen_relative_pos[1]*get_screen_size()[1]-amplitude
            frequency = 30+random.randint(-10,10)
            counter = random.randint(0,frequency)
            
            self.gamestate.images[3].append(Strider(y_pos, amplitude, frequency=frequency, velocity=10,counter=counter))
        Event.execute(self)
class SpawnDroneEvent(Event):
    def __init__(self,gamestate,nmb=1):
        Event.__init__(self)
        self.gamestate = gamestate
        self.nmb = nmb
    def execute(self):
        for i in range(self.nmb):
            y_pos = (0.3%self.gamestate.player.screen_relative_pos[1])*get_screen_size()[1]
            shoot_frequency = 30
            velocity = 10
            self.gamestate.images[3].append(Drone(y_pos, velocity, shoot_frequency))
        Event.execute(self)
class SpawnBossEvent(Event):
    def __init__(self,gamestate):
        Event.__init__(self)
        self.gamestate = gamestate
    def execute(self):
        y_pos = (0.15%self.gamestate.player.screen_relative_pos[1])*get_screen_size()[1]
        self.gamestate.images[3].append(Boss(y_pos))
        Event.execute(self)