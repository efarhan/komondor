'''
Created on 11 janv. 2014

@author: efarhan
'''

from engine.const import log
from engine.physics import add_dynamic_object, add_static_box
from engine.init import get_screen_size
from json_export.json_util import load_json
from event.event_util import add_button
from animation.runner_shooter import RunShootAnimation
from json_export.event_json import load_event

def load_player(player):
    '''
    Set player instance from JSON file
    
    '''
    player_data = load_json(player.filename)

    player.pos = (player_data['pos'][0][0],player_data['pos'][1][0])
    player.screen_relative_pos = (player_data['pos'][0][1],player_data['pos'][1][1])
    player.size = (player_data['size'][0],player_data['size'][1])
    
    try:
        player.layer = player_data['layer']
    except KeyError:
        pass
    
    try:
        player.speed = player_data['speed']
    except KeyError:
        pass
    try:
        if player_data['animation'] == 'RunShootAnimation':
            player.anim = RunShootAnimation(player)
    except KeyError:
        pass
    try:
        player.anim.anim_speed = player_data['anim_speed']
    except KeyError:
        pass
    try:
        player.anim.jump_velocity = player_data['jump_velocity']
    except KeyError:
        pass
    try:
        player.anim.jet_velocity = player_data['jet_velocity']
    except KeyError:
        pass
    player.anim.path = player_data['path']
    player.anim.state_range = player_data['state_range']
    player.anim.path_list = player_data['path_list']
    
    
    physics_pos = (player.pos[0]+player.screen_relative_pos[0]*get_screen_size()[0],
                   player.pos[1]+player.screen_relative_pos[1]*get_screen_size()[1])
    player.body = add_dynamic_object(physics_pos)
    for physic_object in player_data['physic_objects']:
        pos = physic_object['pos']
        size = physic_object['size']
        angle = physic_object['angle']
        data = physic_object['user_data']
        sensor = physic_object['sensor']
        add_static_box(pos, size, angle, data, sensor, body=player.body)
    for player_action in player_data['player_actions'].items():
        add_button(player_action[0],player_action[1])
    try:
        for event in player_data['event'].items():
            player.event[event[0]] = load_event(event[1])
    except KeyError:
        pass
    return True

def save_player(player):
    pass
    
    