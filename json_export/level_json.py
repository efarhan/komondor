'''
Created on 11 janv. 2014

@author: efarhan
'''
import json
from game_object.player import Player
from game_object.physic_object import AngleSquare
from engine.const import log,path_prefix
from game_object.image import Image
from json_export.json_util import load_json
from json_export.event_json import load_event
from game_object.building import Building, MetaBuilding
from game_object.wall import Wall, Firewall
from levels.gui import GUI
from game_object.obstacles import Crouch, JumpObstacle

def load_level(level):
    ''' 
    Import a level with:
    
    -Physics static object
    -Images with or without animation
    -IA (if any)
    -Player position, size, etc... but not recreate the player!!!
    '''
    level_data = load_json(level.filename)
    if level_data:
        
        GUI.__init__(level,level_data['gui'])
        level.player = Player(path_prefix+level_data['player'])
        level.bg_color = level_data['background_color']
        try:
            level.show_mouse = level_data['show_mouse']
        except KeyError:
            pass
        try:
            for checkpoint in level_data['checkpoints']:
                level.checkpoints.append(checkpoint)
        except KeyError:
            pass
        try:
            level.use_physics = level_data['physics']
        except KeyError:
            pass
        try:
            level.use_network = level_data['network']
        except KeyError:
            pass
        try:
            for e in level_data["event"].keys():
                level.event[e] = load_event(level_data["event"][e])
                log(e+" "+str(level.event[e]))
        except KeyError:
            pass
        except AttributeError:
            log("Event Attribute should be dictionnary for level with on_init and on_loop keys",1)
            pass
        for physic_object in level_data['physic_objects']:
            if physic_object["type"] == "box":
                
                pos = physic_object["pos"]
                size = physic_object["size"]
                
                sensor = False
                try:
                    sensor = physic_object["sensor"]
                except KeyError:
                    pass
                data = 0
                try:
                    data = physic_object["user_data"]
                except KeyError:
                    pass
                angle = 0
                try:
                    angle = physic_object["angle"]
                except KeyError:
                    pass
                level.physic_objects.append(AngleSquare(pos, size, angle, data, sensor))
        for image_data in level_data['images']:
            image = None
            if image_data["type"] == "Image":
                pos = image_data["pos"]
                size = None
                try:
                    size = image_data["size"]
                except KeyError:
                    pass
                loop = False
                try:
                    loop = image_data['loop']
                except KeyError:
                    pass
                path = path_prefix+image_data["path"]
                angle = 0
                try:
                    angle = image_data["angle"]
                except KeyError:
                    pass
                
                layer = image_data["layer"]
                image = Image(path, pos, None, size, angle)
                image.image_loop = loop
                try:
                    event_path = image_data["event"]
                    image.event = load_event(event_path)
                except KeyError:
                    pass
                try:
                    image.screen_pos_factor = image_data["screen_pos_factor"]
                except KeyError:
                    pass
                if 0 < layer < len(level.images)-1:
                    level.images[layer-1].append(image)
            elif image_data["type"] == "Building":
                pos = image_data["pos"]
                path = path_prefix+image_data["path"]
                layer = image_data["layer"]
                nmb = image_data["nmb"]
                image = Building(path, pos, nmb)
                if 0 < layer < len(level.images)-1:
                    level.images[layer-1].append(image)
                image = MetaBuilding(path, pos, nmb)
                if 0 < layer < len(level.images)-1:
                    level.images[layer-1].append(image)
            elif image_data["type"] == "Wall":
                pos = image_data['pos']
                layer = image_data['layer']
                meta = image_data['meta']
                image_type = ''
                image = None
                try:
                    image_type = image_data['image_type']
                    image = Wall(image_type, pos)
                    if 0 < layer < len(level.images)-1:
                        level.images[layer-1].append(image)
                except KeyError:
                    pass
                if meta:
                    image = Firewall( pos=pos)
                    if 0 < layer < len(level.images)-1:
                        level.images[layer-1].append(image)
            elif image_data["type"] == "Crouch":
                pos = image_data['pos']
                layer = image_data['layer']
                image = Crouch(pos)
                if 0 < layer < len(level.images)-1:
                    level.images[layer-1].append(image)
            elif image_data["type"] == "JumpObstacle":
                pos = image_data['pos']
                layer = image_data['layer']
                image_type = image_data['image_type']
                image = JumpObstacle(image_type, pos)
                if 0 < layer < len(level.images)-1:
                    log('Added jump obstacle')
                    level.images[layer-1].append(image)
                else:
                    log('Layer should be between 1 and 5',1)
            try:
                image.meta = image_data["meta"]
            except KeyError:
                pass
          
        return True
    return False
def save_level(level):
    
    
    level_data = {}
    level_data['player'] = level.player.filename
    level_data['background_color'] = level.bg_color
    level_data['physic_objects'] = []
    for physic_object in level.physic_objects:
        if physic_object.__class__ == AngleSquare:
            obj = {}
            obj['type'] = 'box'
            obj['pos'] = [physic_object.pos[0],physic_object.pos[1]]
            obj['size'] = [physic_object.size[0],physic_object.size[1]]
            obj['sensor'] = physic_object.sensor
            obj['user_data'] = physic_object.data
            obj['angle'] = physic_object.angle
            level_data['physic_objects'].append(obj)
    i = 1 #layer
    level_data['images'] = []
    for layer in level.images:
        for image in layer:
            obj = {}
            obj['type'] = 'Image'
            obj['layer'] = i
            obj['path'] = image.path
            obj['size'] = [image.size[0],image.size[1]]
            obj['pos'] = [image.pos[0],image.pos[1]]
            obj['angle'] = image.angle
            level_data['images'].append(obj)
        i+=1
    file = open(level.filename,mode='w')
    file.write(json.dumps(obj=level_data,indent=4))
    file.close()