'''
TODO: import gui from JSON

Created on Feb 26, 2014

@author: efarhan
'''
from json_export.json_util import load_json
from game_object.text import TutorialsText

def load_gui(gui,filename):
    gui_data = load_json(filename)
    for tutorial in gui_data['tutorials_text']:
        text = tutorial['text']
        x_from = tutorial['from']
        x_to = tutorial['to']
        gui.tutorials_text.append(TutorialsText(
                                                gui.text_pos, 
                                                gui.text_size, 
                                                gui.font, 
                                                text, 
                                                gui.tutorial_color,
                                                x_from=x_from, 
                                                x_to=x_to))
    return None